//
//  VolumeControlTest.swift
//  SportRadarNalogaTests
//
//  Created by Sandi Mihelic on 28/05/2018.
//  Copyright © 2018 Sandi Mihelic. All rights reserved.
//

import XCTest

class VolumeControlTest: XCTestCase {
    var volumeControl: VolumeControl?
    
    override func setUp() {
        super.setUp()
        volumeControl = VolumeControl(frame: CGRect(x: 0, y: 0, width: 50, height: 150))
        volumeControl?.lines = 10
        self.volumeControl?.value = 0.5
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testVolumeControlIsInitialized() {
        XCTAssertNotNil(self.volumeControl)
    }
    
    func testIsValueIsInRange() {
        XCTAssertTrue(self.volumeControl?.isValueIsInRange(value: 0.5, min: 0, max: 1) == 0.5)
        XCTAssertTrue(self.volumeControl?.isValueIsInRange(value: -1, min: 0, max: 1) == 0)
        XCTAssertTrue(self.volumeControl?.isValueIsInRange(value: 2, min: 0, max: 1) == 1)
    }
        
    func testGetLineHeight() {
        XCTAssertTrue(self.volumeControl?.getSquareHeight(height: 150) == 10)
    }
    
    func testYPositionToIndex() {
       XCTAssertTrue(self.volumeControl?.yPositionToIndex(y: 0, height: 150) == 0)
       XCTAssertTrue(self.volumeControl?.yPositionToIndex(y: 75, height: 150) == 5.0)
       XCTAssertTrue(self.volumeControl?.yPositionToIndex(y: 150, height: 150) == 10.0)
    }
    
    func testCalculateIndexToProcentage() {
        XCTAssertTrue(self.volumeControl?.calculateIndexToProcentage(0) == 0)
        XCTAssertTrue(self.volumeControl?.calculateIndexToProcentage(5) == 0.5)
        XCTAssertTrue(self.volumeControl?.calculateIndexToProcentage(10) == 1.0)
        XCTAssertTrue(self.volumeControl?.calculateIndexToProcentage(3) == 0.3)
        XCTAssertTrue(self.volumeControl?.calculateIndexToProcentage(-1) == 0)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
