//
//  ViewController.swift
//  SportRadarNaloga
//
//  Created by Sandi Mihelic on 28/05/2018.
//  Copyright © 2018 Sandi Mihelic. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var linesInput: UITextField!
    @IBOutlet weak var volumeInput: UITextField!
    @IBOutlet weak var volumeControl: VolumeControl!
    @IBOutlet weak var volumeLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        volumeInput.tag = 1
        linesInput.tag = 0
        
        volumeInput.delegate = self
        linesInput.delegate = self
        
        volumeInput.keyboardType = .numbersAndPunctuation
        linesInput.keyboardType = .numbersAndPunctuation
        
        let tapGestue = UITapGestureRecognizer(target: self, action: #selector(ViewController.hideKeyboard))
        self.view.addGestureRecognizer(tapGestue)
        self.volumeControl.delegate = self
        self.didChange(value: self.volumeControl.value)        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func hideKeyboard() {
        self.view.endEditing(true)
    }
    
    func checkIfValueIsInRange(value: Int, min: Int, max: Int) -> Int {
        if value > max { return max }
        if value < min { return min }
        return value
    }

    @IBAction func setVolumeAction() {
        guard var volumeNumber = Int(self.volumeInput.text ?? "") else {
            return
        }
        volumeNumber = checkIfValueIsInRange(value: volumeNumber, min: 0, max: 100)
        self.volumeInput.text = "\(volumeNumber)"
        self.volumeControl.value = CGFloat(volumeNumber) / 100
    }
    
    @IBAction func setLinesAction() {
        guard var linesNumber = Int(self.linesInput.text ?? "") else {
            return
        }
        linesNumber = checkIfValueIsInRange(value: linesNumber, min: 0, max: 100)
        self.linesInput.text = "\(linesNumber)"
        self.volumeControl.lines = linesNumber
    }
    
    @objc func doneClicked() {
        self.view.endEditing(true)
    }
}

extension ViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag == 0 {
            self.setLinesAction()
        } else {
            self.setVolumeAction()
        }
        
        textField.resignFirstResponder()
        return false
    }
}

extension ViewController: VolumeControlDelegate {
    func didChange(value: CGFloat) {
        let volumeProcentage = Int(value * 100)
        self.volumeInput.text = "\(volumeProcentage)"
        self.volumeLabel.text = "Volume set at: \(volumeProcentage)%"
        self.linesInput.text = "\(self.volumeControl.lines)"
    }
}

