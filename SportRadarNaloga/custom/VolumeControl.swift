//
//  VolumeControll.swift
//  SportRadarNaloga
//
//  Created by Sandi Mihelic on 28/05/2018.
//  Copyright © 2018 Sandi Mihelic. All rights reserved.
//

import UIKit
import Foundation

protocol VolumeControlDelegate: class {
    func didChange(value: CGFloat)
}

@IBDesignable
class VolumeControl: UIView {
    
    weak var delegate: VolumeControlDelegate?
    var spacing: CGFloat = 5
    
    lazy var test = { [spacing] () -> Void in
        print(self.spacing)
    }
    
    @IBInspectable var normalColor: UIColor = UIColor.gray {
        didSet {
            self.setNeedsDisplay()
        }
    }

    @IBInspectable var color: UIColor = .blue {
        didSet {
            self.setNeedsDisplay()
        }
    }

    @IBInspectable var value: CGFloat = 0 {
        didSet {
            self.setNeedsDisplay()
            self.delegate?.didChange(value: value)
        }
    }

    @IBInspectable var lines: Int = 2 {
        didSet {
            self._lines = CGFloat(lines)
            self.setNeedsDisplay()
        }
    }
    
    private var _lines: CGFloat = 0

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }

    func setup() {
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(getPointerPosition(gesture:)))
        self.addGestureRecognizer(panGesture)
    }

    override func draw(_ rect: CGRect) {
        let lineHeight = self.getSquareHeight(height: rect.height)
        let lineWithSpaceHeight = self.spacing + lineHeight
        var drawingPositionY = rect.height - lineHeight
        
        _ = [Int](0..<self.lines).map { (index) in
            let lineColor = self.getColor(index: index)
            self.drawSquare(self.generateLineRect(y: drawingPositionY, rect: rect), color: lineColor)
            drawingPositionY -= lineWithSpaceHeight
        }
    }

    @objc func getPointerPosition(gesture: UIPanGestureRecognizer) {
        let transformedY = self.transformY(y: gesture.location(in: self).y)
        let index = transformedY <= 0 ? 0 : self.yPositionToIndex(y: transformedY, height: self.bounds.height)
        self.value = isValueIsInRange(value: index / _lines, min: 0, max: 1.0)
        self.setNeedsDisplay()
    }
    
    /*
     Reverse y so 0 starts at the bottom
     */
    func transformY(y: CGFloat) -> CGFloat {
        return self.bounds.height - y
    }
    
    func yPositionToIndex(y: CGFloat, height: CGFloat) -> CGFloat {
        let squareHeightWithSpacing = self.getSquareHeight(height: height) + self.spacing
        return (y / squareHeightWithSpacing).rounded(.up)
    }
    
    func isValueIsInRange(value: CGFloat, min: CGFloat, max: CGFloat) -> CGFloat {
        if value > max { return max }
        if value < min { return min }
        return value
    }
    
    func generateLineRect(y: CGFloat, rect: CGRect) -> CGRect {
       return CGRect(x: 0, y: y, width: rect.width, height: self.getSquareHeight(height: rect.height))
    }
    
    func getColor(index: Int) -> UIColor {
        return self.calculateIndexToProcentage(index + 1) <= self.value ? self.color : self.normalColor
    }
    
    func calculateIndexToProcentage(_ index: Int) -> CGFloat {
        let index = CGFloat(index)
        return self.isValueIsInRange(value: ( index / _lines ), min: 0, max: 1)
    }
    
    func drawSquare(_ rect: CGRect, color: UIColor) {
        let bpath:UIBezierPath = UIBezierPath(rect: rect)
        color.set()
        bpath.fill()
    }
    
    func getSquareHeight(height: CGFloat) -> CGFloat {
        return ( height - ( _lines * self.spacing ) ) / _lines
    }
}
